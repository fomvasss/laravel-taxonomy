# Changelog LaravelTaxonomy

## 1.0.2 - 2017-12-22

- Remove commands

## 1.0.1 - 2017-12-14

- Remove commands

## 1.0.0 - 2017-12-12

- Initial release, make package
